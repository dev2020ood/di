﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.DTO
{
    public class ResponseError:Response
    {
        public string ErrorMsg { get; set; }
    }
}
