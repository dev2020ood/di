using Contracts;
using DI;

using NUnit.Framework;
using System;
using System.Reflection;
using TicTacToeContracts.Interface.Players;

namespace InfraTests
{
    public class Tests
    {
        IResolver _resolver; 
        [SetUp]
        public void Setup()
        {
            _resolver = new Resolver(@"..\dlls");
        }

        [Test]
        public void Test1()
        {
            
            var player = _resolver.Resolve<IHumanPlayer>();
            Assert.IsInstanceOf(typeof(IHumanPlayer), player); 
            

        }
    }
}